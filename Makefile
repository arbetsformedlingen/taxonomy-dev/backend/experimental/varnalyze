.PHONY: test scrape test-pyvarnalyze

test:
	lein test

scrape:
	lein run scrape scraped_logs newer

test-pyvarnalyze:
	cd pyvarnalyze && poetry run python3 -m unittest discover
