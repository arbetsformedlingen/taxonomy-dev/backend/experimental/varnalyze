# Varnalyze

Varnalyze is a tool for:

* Mining and analyzing logs from opensearch
* Simulating Varnish

Using it amounts to:

1. Downloading logs to be analyzed (`make scrape`)
2. Running a simulation on the logs (see `src/clj/varnalyze/simulate.clj`)
3. Plotting the results (see `pyvarnalyze/pyvarnalyze/analyze.py`)

## Usage

**Scraping data**

Start by scraping some logs to work on. Open the page on OpenSearch with results that you are interested in, for instance [this url](resources/opensearch_web_url.txt). In the web browser, open **web developer tools** and the the Network tab. Make sure that *XHR* is enabled. Look at the `POST https://opensearch.jobtechdev.se/internal/search/opensearch` request that has a response of roughly 3 MB.

Once you have found the request that you are interested in, right click on the request and *copy headers* to the file [`resources/query_headers.txt`](resources/query_headers.txt) and *copy post data* to [`resources/query_post_data.json`](resources/query_post_data.json).

Type `make scrape` to scrape the logs.

**Simulating Varnish using scraped data**

Open a Clojure repl and load the namespace `varnalyze.simulate`. At the bottom of the file [`src/clj/varnalyze/simulate.clj`](src/clj/varnalyze/simulate.clj), there is a rich comment. Evaluate the forms in that comment, ending with `(run-simulation1 prep cfg)`. This will run the simulation on the data that you saved.

**Visualizing the results**

In the [`pyvarnalyze`](pyvarnalyze/) directory, do `poetry shell` (and maybe `poetry install` on the first load). Open a Python3 repl and load the file [`pyvarnalyze/pyvarnalyze/analyze.py`](pyvarnalyze/pyvarnalyze/analyze.py). At the bottom of that file is code for loading visualizing the results.
