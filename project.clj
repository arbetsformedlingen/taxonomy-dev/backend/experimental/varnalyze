(defproject varnalyze "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :source-paths ["src/clj"]
  :java-source-paths ["src/java"]
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [clj-http/clj-http "3.12.3"]
                 [org.clojure/data.json "2.4.0"]]
  :main vclgen.api
  :repl-options {:init-ns vclgen.core})
