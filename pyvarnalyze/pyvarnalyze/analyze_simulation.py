import importlib
import math
import json
import matplotlib.pyplot as plt
import collections
import tempfile
from pyvarnalyze.filesystem_location import Location
import subprocess
import pyvarnalyze.common as common; importlib.reload(common)
from pathlib import Path
import os
import re
import datetime
import numpy as np
import pyvarnalyze.fit as fit; importlib.reload(fit)
import urllib

location = Location.default()

        
class ReportWriter:
    def __init__(self, prefix=None):
        self._prefix = prefix or ""

    def deeper(self):
        return ReportWriter(self._prefix + "  ")
    
    def writeln(self, x):
        print(f"{self._prefix}{x}")

class TimeSampler:
    def __init__(self, period, factor):
        self._period = period
        self._factor = factor
        self._next = None
        self._offset = None

    def sample(self, dt):
        t = dt.timestamp()
        if self._next is None:
            self._next = t + self._period
            self._period *= self._factor
            self._offset = t
            return True
        elif self._next <= t:
            while self._next <= t:
                self._next += self._period
                self._period *= self._factor
            return True
        else:
            return False
            

class DatasetFile:
    def __init__(self, path, timestamp, approx_count):
        self.path = path
        self.timestamp = timestamp
        self.approx_count = approx_count

def with_utc(dt):
    return dt.replace(tzinfo=datetime.timezone.utc)
        
def parse_timestamp(s):
    assert(isinstance(s, str))
    assert(s[-1] == "Z")
    return with_utc(datetime.datetime.fromisoformat(s[0:-1]))

class SimulationDataset:
    def __init__(self, root_path, files):
        self._root_path = root_path
        self._files = files

    def approx_count(self):
        n = 0
        for f in self._files:
            n += f.approx_count
        return n
        
    def __repr__(self):
        first = self._files[0]
        last = self._files[-1]
        return f"SimulationDataset(path={self._root_path}, from {first.timestamp} to {last.timestamp}, approx count {self.approx_count()}"

    def __iter__(self):
        for f in self._files:
            data = common.read_json(f.path)
            for x in data:
                x["timestamp"] = parse_timestamp(x["timestamp"])
                yield x

def is_dataset(x):
    return isinstance(x, SimulationDataset)

pattern = r"data_n(\d+)_(\d+)T(\d+)_(\d+)Z.json"
        
def analyze_simulation_file(path):
    m = re.match(pattern, path.name)
    if m is None:
        return None

    approx_count = int(m.group(1))
    date = m.group(2)
    tod = m.group(3)
    ms = m.group(4)

    t = with_utc(datetime.datetime(int(date[0:4]), int(date[4:6]), int(date[6:8]),
                                   int(tod[0:2]), int(tod[2:4]), int(tod[4:6]),
                                   int(ms)))
    return DatasetFile(path, t, approx_count)

def path_key(x):
    return tuple(x["simulation-handling"]["path"])
        
def simulation_dataset(root_path):
    assert(isinstance(root_path, Path))
    assert(root_path.is_dir())
    files = [f
             for child in os.listdir(root_path)
             for f in [analyze_simulation_file(root_path.joinpath(child))]
             if f is not None]
    return SimulationDataset(root_path, list(sorted(files, key=lambda f: f.timestamp)))

class Config:
    def __init__(self):
        self.sample_period = 60.0
        self.sample_factor = 1.0
        self.log_period = 10000

def compute_hit_rate(m):
    total = sum(m.values())
    return m.get("hit", 0)/total


class UniqueDataCounter:
    def __init__(self):
        self._freqs = {}
        self._size = 0
        self._total_count = 0
        self._total_size = 0

    def size(self):
        return self._size

    def count(self):
        return len(self._freqs)
        
    def add(self, key, size):
        self._total_count += 1
        self._total_size += size
        if key not in self._freqs:
            self._size += size
        common.step_counter(self._freqs, key)

    def __repr__(self):
        return f"UniqueDataCounter(count={self.count()}, size={self.size()})"

    def freqs(self):
        return list(sorted(self._freqs.items(), key=lambda kv: -kv[1]))

    def plot_request_distrib(self, ax):
        freqs = self.freqs()
        X = []
        Y = []
        for i, (k, n) in enumerate(freqs):
            if 1 < n:
                X.append(str(i))
                Y.append(n)
            else:
                X.append("U")
                Y.append(len(freqs) - i)
                break
        ax.bar(X, Y)
    
    def report(self, writer):
        freqs = self.freqs()
        writer.writeln(f"Unique count: {self.count()}/{self._total_count}")
        writer.writeln(f"Unique size: {self.size()}/{self._total_size}")
        writer.writeln(f"Most common: {freqs[0][1]}")

class Acc:
    def __init__(self, key, config):
        self._key = key
        self._sampler = TimeSampler(config.sample_period, config.sample_factor)
        self._hit_freqs = {}
        self._obs_hit_freqs = {}
        self._times = []
        self._hit_rate = []
        self._obs_hit_rate = []
        self._unique_data_counter = UniqueDataCounter()
        self._unique_count = []
        self._unique_size = []
        self._used_rate = []
        self._last_record = None

    def key(self):
        return self._key

    def __repr__(self):
        return f"Acc(key={self._key}, samples={len(self._times)})"

    def accumulate(self, record):
        dt = record["timestamp"]
        common.step_counter(self._hit_freqs, record["simulation-handling"]["type"])
        common.step_counter(self._obs_hit_freqs, record["handling"])
        self._unique_data_counter.add(record["cache-key"], record["size"])
        self._last_record = record
        if self._sampler.sample(dt):
            hdata = record["simulation-handling"]
            self._times.append(dt)
            self._hit_rate.append(compute_hit_rate(self._hit_freqs))
            self._obs_hit_rate.append(compute_hit_rate(self._obs_hit_freqs))
            self._unique_count.append(self._unique_data_counter.count())
            self._unique_size.append(self._unique_data_counter.size())

            stats = hdata["data"]
            size = stats["size"]
            self._used_rate.append(stats["used"]/size if 0 < size else 0)

    def last_record(self):
        return self._last_record
            
    def duration(self):
        l = self._times[0]
        u = self._times[-1]
        return u.timestamp() - l.timestamp()

    def time_resolution(self):
        return common.compute_resolution(self.duration())

    def timeplot_label(self):
        (unit, x) = self.time_resolution()
        return f"Time ({unit})"

    def timestamps_in_resolution(self):
        (unit_name, res) = self.time_resolution()
        return [x/res for x in self.timestamps_seconds()]
    
    def timestamps_seconds(self):
        return [t.timestamp() - self._times[0].timestamp() for t in self._times]
            
    def plot_hit_rates(self, ax):
        timestamps = self.timestamps_in_resolution()
        sim, = ax.plot(timestamps, self._hit_rate, label="sim")
        obs, = ax.plot(timestamps, self._obs_hit_rate, label="obs")
        ax.legend(handles=[sim, obs])

    def plot_unique(self, ax, which):
        ax.plot(self.timestamps_in_resolution(), {"count": self._unique_count, "size": self._unique_size}[which])

    def plot_request_distrib(self, ax):
        self._unique_data_counter.plot_request_distrib(ax)

    def plot_used_rate(self, ax):
        ax.plot(self.timestamps_in_resolution(), self._used_rate)

    def report(self, writer):
        writer.writeln(f"Accumulator {self._key}")
        self._unique_data_counter.report(writer.deeper())

    def unique_data_counter(self):
        return self._unique_data_counter

def key_indexing(ks):
    return {k:i for i, k in enumerate(sorted(ks))}

def path_dimensions(paths):
    n = None
    for p in paths:
        m = len(p)
        if n is None:
            n = m
        else:
            assert(n == m)
    dims = [set() for i in range(n)]
    for p in paths:
        for i, x in enumerate(p):
            dims[i].add(x)
    return [sorted(x) for x in dims]

def log_range(lower, upper, count):
    return np.exp(np.linspace(math.log(lower), math.log(upper), count))

class CommonAcc:
    def __init__(self, sizes=None):
        self._sizes = sizes or []

    def accumulate(self, x):
        self._sizes.append(x["size"])

    def plot_count_hist(self):
        plt.figure()
        plt.hist(self._sizes, bins = log_range(1, 100*common.mega, 50))
        plt.gca().set_xscale("log")
        plt.gca().set_yscale("log")
        plt.show()

    def rebuild(src):
        return CommonAcc(src._sizes)

class Analysis:
    def __init__(self, common_acc, acc_map):
        self._common_acc = common_acc
        self._acc_map = acc_map

    def common(self):
        return self._common_acc
        
    def accs(self):
        return sorted(self._acc_map.values(), key=lambda x: x.key())

    def path_dimensions(self):
        return path_dimensions(self._acc_map.keys())

    def plot_every_bucket(self, f, xlabel, ylabel):
        dims = path_dimensions(self._acc_map.keys())
        (pod_inds, bucket_inds) = [key_indexing(x) for x in dims]
        accs = self.accs()
        fig, axs = plt.subplots(len(pod_inds), len(bucket_inds))
        max_row = len(pod_inds)-1
        max_col = len(bucket_inds)-1
        for i, acc in enumerate(accs):
            (pod, bucket) = acc.key()
            row = pod_inds[pod]
            col = bucket_inds[bucket]
            ax = axs[row, col]
            f(ax, acc)
            if col == 0:
                ax.set_ylabel(ylabel)
            if row == max_row:
                ax.set_xlabel(xlabel)
            if row == 0:
                ax.set_title(bucket)
        plt.show()

    def timeplot_label(self):
        return self.accs()[0].timeplot_label()
        
    def plot_hit_rates(self):
        def plot_bucket(ax, acc):
            acc.plot_hit_rates(ax)
        self.plot_every_bucket(plot_bucket, self.timeplot_label(), "Hit rate")

    def plot_unique(self, which):
        def plot_bucket(ax, acc):
            acc.plot_unique(ax, which)
        self.plot_every_bucket(plot_bucket, self.timeplot_label(), f"Unique {which}")

    def plot_request_distrib(self):
        def plot_bucket(ax, acc):
            acc.plot_request_distrib(ax)
        self.plot_every_bucket(plot_bucket, "Order", "Freq")

    def plot_used_rate(self):
        def plot_bucket(ax, acc):
            acc.plot_used_rate(ax)
        self.plot_every_bucket(plot_bucket, self.timeplot_label(), "Used rate")

    def accs_per_bucket(self):
        return {bucket_key:[v for (k, v) in kv_group]
                for bucket_key, kv_group in common.group_by(lambda kv: kv[0][1], self._acc_map.items()).items()}

    def bucket_stats(self, bucket_cfg):
        bucket_cfg_map = {c["name"]:c for c in bucket_cfg}
        (pod_keys, bucket_keys) = self.path_dimensions()
        acc_map = self.accs_per_bucket()
        data = []

        pod_factor = 1.0/len(pod_keys)
        
        def report_inserted(bucket_key, dst):
            inserted_bytes = dst['inserted-bytes']
            print(f"Inserted bytes bucket {bucket_key}: {common.format_size(inserted_bytes)}")

        overall = {}
        for bucket_key in bucket_keys:
            dst = {"key": bucket_key}
            for acc in acc_map[bucket_key]:
                bucket_data = acc.last_record()["simulation-handling"]["data"]
                common.add_to_map(dst, bucket_data, pod_factor)
                common.add_to_map(overall, bucket_data, pod_factor)
            dst["cfg"] = bucket_cfg_map[bucket_key]
            report_inserted(bucket_key, dst)
            data.append(dst)
        report_inserted("Overall", overall)
        return data
        
    def plot_bucket_stats(self, bucket_cfg):
        data = self.bucket_stats(bucket_cfg)
        rows = 1
        fig, axs = plt.subplots(1, 2)


        # Plot per bucket bytes
        ax = axs[0]
        ax.bar([x["key"] for x in data], [x["inserted-bytes"] for x in data])
        ax.set_yscale("log")
        ax.set_xlabel("Bucket")
        ax.set_ylabel("Inserted bytes")
        ax.set_title("Inserted bytes per bucket")

        # Plot per inserted vs req size
        ax = axs[1]
        X = [x["cfg"]["upper"] for x in data]
        Y = [x["inserted-bytes"] for x in data]
        ax.scatter(X, Y)
        ax.set_xlabel("Upper request size")
        ax.set_ylabel("Used size")
        ax.set_xscale("log")
        ax.set_yscale("log")

        for x in data:
            key = x["key"]
            inserted = x["inserted-bytes"]
            upper_req_size = x["cfg"]["upper"]
            print(f"Bucket {key}: {inserted/upper_req_size}, inserted: {common.format_size(inserted)}  upper req size: {common.format_size(upper_req_size)}  log/log: {math.log(inserted)/math.log(upper_req_size)} ")


        # Show it
        plt.show()
            
        
        
    def report(self, writer=None):
        writer = writer or ReportWriter()
        writer.writeln("Report per bucket:")
        for acc in self.accs():
            acc.report(writer)
        

def analyze_records(records, config):
    n = records.approx_count() if is_dataset(records) else None
    bucket_accs = {}
    common_acc = CommonAcc()
    for i, x in enumerate(records):
        if i % config.log_period == 0:
            print(f"Process record {i} of {'?' if n is None else n}")
        key = path_key(x)
        if not(key in bucket_accs):
            bucket_accs[key] = Acc(key, config)
        bucket_accs[key].accumulate(x)
        common_acc.accumulate(x)
    return Analysis(common_acc, bucket_accs)    

if False:
    ds = simulation_dataset(location.overall_root().joinpath("simulations", "testsim1"))
    print(ds)

if False:
    cm0 = fit.fit_bucket_capacity_model(analysis.bucket_stats(bucket_cfg))
    print("BEFORE")
    cm0.report()
    print("AFTER")
    cm = 6*cm0
    cm.report()
    after_scaling = cm.evaluate_total_capacity(1.0*common.giga, bucket_cfg)
    print(f"Total cap: {common.format_size(after_scaling)}")

if False: # Analyze path frequencies
    bucket_cfg_path = location.overall_root().parent.joinpath(
        "openshift-varnish", "buckets", "bucket_data.json")
    bucket_cfg = common.read_json(bucket_cfg_path)
    
    x = next(iter(ds))

    subds = common.take_n(10000, ds)
    #subds = ds

    analysis = analyze_records(subds, Config())

if False:
    analysis.plot_bucket_stats(bucket_cfg)
if False:    
    analysis.plot_hit_rates()
if False:
    analysis.plot_used_rate()
if False:
    analysis.plot_unique("count")
if False:
    analysis.plot_request_distrib()
    
if False:

    accs = analysis.accs()
    acc = accs[3]
    udc = acc._unique_data_counter

    kv = sorted(udc._freqs.items(), key=lambda kv: -kv[1])

    for i, (k, v) in enumerate(kv):

        print(f"{k}: {v}")
        
        if 3 < i:
            break
