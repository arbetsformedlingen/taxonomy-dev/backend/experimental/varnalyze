import json

kilo = 1024
mega = kilo*kilo
giga = kilo*mega

time_breakdown = [(60, "seconds"),
                  (60, "minutes"),
                  (24, "hours"),
                  (7, "days"),
                  (0, "weeks")]

def decompose_seconds(t):
    parts = []
    t = int(round(t))
    unit_seconds = 1
    for (n, unit) in time_breakdown:
        if t <= 0:
            break
        def add_part(x):
            parts.append((x, unit, unit_seconds))
        if n == 0:
            add_part(t)
        else:
            add_part(t % n)
            t = t // n
            unit_seconds *= n
    return list(reversed(parts))

def compute_resolution(t):
    parts = decompose_seconds(t)
    print(parts)
    n = len(parts)
    if n <= 1:
        return ("seconds", 1)
    else:
        (n, unit, unit_seconds) = parts[1]
        return (unit, unit_seconds)

def seconds_to_string(t):
    parts = [p for p in decompose_seconds(t) if 0 < p[0]]
    if len(parts) == 0:
        return "0 seconds"
    return ", ".join([f"{t} {unit}" for i, (t, unit, _) in enumerate(parts) if i < 2])

def format_size(size):
    for unit in ["B", "KB", "MB", "GB"]:
        if size < 500:
            return f"{size:.2f} {unit}"
        size = size/1024.0

def write_json(filename, data):
    with open(filename, "w") as f:
        json.dump(data, f)
        
def read_json(filename):
    with open(filename, "r") as f:
        return json.load(f)

def group_by(f, elements):
    dst = {}
    for x in elements:
        k = f(x)
        if not(k in dst):
            dst[k] = []
        dst[k].append(x)
    return dst

def step_counter(dst, x):
    dst[x] = dst.get(x, 0) + 1

def frequencies(elements):
    dst = {}
    for x in elements:
        step_counter(dst, x)
    return dst

def take_n(n, src):
    for i, x in enumerate(src):
        if i < n:
            yield x
        else:
            break

def add_to_map(dst, x, f):
    for k, v in x.items():
        if k not in dst:
            dst[k] = 0
        dst[k] += f*v
    
