from pathlib import Path

class Location:
    def __init__(self, this_path):
        self._this_path = this_path

    def python_root(self):
        return self._this_path.parent.parent
        
    def overall_root(self):
        return self.python_root().parent

    def default():
        return Location(Path(__file__))
