import pyvarnalyze.common as common
import math
import numpy as np

def format_poly_term(c, x, n):
    if n == 0:
        return common.format_size(c)
    elif n == 1:
        return f"{c}*{x}"
    else:
        return f"{c}*{x}^{n}"
    

def x_powers(x, n):
    y = 1
    dst = []
    for i in range(n):
        dst.append(y)
        y *= x
    return dst
    
class PolyFit:
    def __init__(self, coefs):
        self._coefs = coefs

    def __call__(self, x):
        dst = 0
        for c in reversed(self._coefs):
            dst = x*dst + c
        return dst

    def __repr__(self):
        return " + ".join([format_poly_term(c, "x", i) for i, c in enumerate(self._coefs)])

    def fit(n, XYW):
        A = []
        B = []
        for x, y, w in XYW:
            A.append([w*p for p in x_powers(x, n)])
            B.append([w*y])
        sol, res, rank, s = np.linalg.lstsq(A, B)
        return PolyFit(sol.flatten())

    def __rmul__(self, f):
        return PolyFit([f*c for c in self._coefs])

class LogLogLineFit:
    def __init__(self, k, m):
        self._f = math.exp(m)
        self._k = k
        self._m = m

    def __call__(self, x):
        return self._f*math.exp(self._k*math.log(x))

    def fit(XYW):
        A = []
        B = []
        for x, y, w in XYW:
            A.append([math.log(x), 1.0])
            B.append([math.log(y)])
        sol, res, rank, s = np.linalg.lstsq(A, B)
        (k, m) = sol.flatten()
        return LogLogLineFit(k, m)

    def __repr__(self):
        return f"e^({self._k}*x + {self._m})"

    def __rmul__(self, f):
        return LogLogLineFit(self._k, self._m + math.log(f))
        

class CapacityModel:
    def __init__(self, f, fit_data):
        self.f = f
        self.fit_data = fit_data

    def report(self):
        print("---Report")
        print(f"Function: {self.f}")
        for x in self.fit_data:
            key = x["key"]
            upper = x["upper-req-size"]
            inserted = x["inserted-size"]
            print(f"Bucket {key}: upper={common.format_size(upper)} inserted={common.format_size(inserted)} fit={common.format_size(self.f(upper))}")
        print("\n\n")

    def evaluate_total_capacity(self, max_bucket_capacity, bucket_cfg):
        total = 0
        for x in bucket_cfg:
            total += min(max_bucket_capacity, self.f(x['upper']))
        return total

    def __rmul__(self, x):
        return CapacityModel(x*self.f, self.fit_data)

def fit_bucket_capacity_model(bucket_stats):
    n = len(bucket_stats)
    fit_data = []
    XYW = []
    for x in bucket_stats:
        upper = x["cfg"]["upper"]
        f = 1.0
        inserted = x["inserted-bytes"]
        XYW.append((upper, inserted, f))
        fit_data.append({"key": x["key"], "upper-req-size": upper, "inserted-size": inserted})
    #f = PolyFit.fit(2, XYW)
    f = LogLogLineFit.fit(XYW)
    return CapacityModel(f, fit_data)

    
