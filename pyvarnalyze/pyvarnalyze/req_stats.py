import pyvarnalyze.analyze_simulation as analyze
import pyvarnalyze.common as common
from urllib.parse import urlparse, parse_qs
import matplotlib.pyplot as plt
from pathlib import Path
import os
import csv
import datetime

# https://github.com/ivelum/graphql-py/blob/master/graphql/ast.py
from graphql.parser import GraphQLParser, Document, Query, Field, Argument, FragmentSpread, TypeCondition, FragmentDefinition, NamedType

gparser = GraphQLParser()

class GraphqlNormalizer:
    def __init__(self, isd=True):
        self._included_specific_data = isd

    def result(self, common, specific):
        if self._included_specific_data:
            return common | specific
        else:
            return common

    def normalize_list(self, coll):
        return [self.normalize(x) for x in coll]
    
    def normalize(self, x):
        def operation_definition(type0, x):
            return self.result({"type": type0,                    
                                "selections": self.normalize_list(x.selections),
                                "variable_definitions": self.normalize_list(x.variable_definitions),
                                "directives": self.normalize_list(x.directives)},
                               {"name": x.name})

        if isinstance(x, Document):
            return {"type": "Document",
                    "definitions": self.normalize_list(x.definitions)}
        elif isinstance(x, Query):
            return operation_definition("Query", x)
        elif isinstance(x, Field):
            return self.result({"type": "Field",
                                "arguments": self.normalize_list(x.arguments),
                                "directives": self.normalize_list(x.directives),
                                "selections": self.normalize_list(x.selections)},
                               {"name": x.name,
                                "alias": x.alias})
        elif isinstance(x, Argument):
            return self.result({"type": "Argument",
                                "name": x.name},
                               {"value": x.value})
        elif isinstance(x, FragmentSpread):
            return self.result({"type": "FragmentSpread",
                                "directives": self.normalize_list(x.directives)},
                               {"name": x.name})
        elif isinstance(x, FragmentDefinition):
            return self.result({"type": "FragmentSpread",
                                "directives": self.normalize_list(x.directives),
                                "selections": self.normalize_list(x.selections),
                                "type_condition": self.normalize(x.type_condition)},
                               {"name": x.name})
        elif isinstance(x, TypeCondition):
            return self.result({"type": "TypeCondition"},
                               {"name": x.name})
        elif isinstance(x, NamedType):
            return self.result({"type": "NamedType"},
                               {"name": x.name})
        else:
            raise NotImplementedError(f"Cannot turn type {type(x)} into data")

gql_form_normalizer = GraphqlNormalizer(False)

def string_from_normalized_data(x):
    if isinstance(x, str):
        return f'"{x}"'
    elif isinstance(x, int) or isinstance(x, float):
        return f"{x}"
    elif isinstance(x, list):
        inner = ",".join(sorted([string_from_normalized_data(y) for y in x]))
        return f"[{inner}]"
    if isinstance(x, dict):
        pairs = sorted([(k, string_from_normalized_data(v))
                        for (k, v) in x.items()])
        inner = ", ".join([k + ':' + v for (k, v) in pairs])
        return "{" + inner + "}"


def fixup_graphql(x0):
    x = x0.strip()
    if x.startswith("{"):
        return f"query MyQuery {x}"
    else:
        return x0
    
    
def normed_graphql_feature(y):
    try:
        gql_ast = gparser.parse(fixup_graphql(y))
    except Exception as e:
        #print("GRAPHQL")
        #print(y)
        print("Failed to parse Graphql")
        return f"FAILED: {y}"
    normed_data = gql_form_normalizer.normalize(gql_ast)
    return string_from_normalized_data(normed_data)


def req_stats_query_keys(req_stats):
    return "/".join(sorted(req_stats.parsed_qs.keys()))

def req_stats_graphql(req_stats):
    if req_stats.parsed_url.path == '/v1/taxonomy/graphql' and req_stats_query_keys(req_stats) == 'query':
        return req_stats.parsed_qs["query"][0]
    
def basic_aggregation_key(req_stats):
    path = req_stats.parsed_url.path
    qkeys = "/".join(sorted(req_stats.parsed_qs.keys()))
    gqls = ""
    g = req_stats_graphql(req_stats)
    if g is not None:
        gqls = normed_graphql_feature(g)
    return (path, qkeys, gqls)


def pprint_list(coll):
    print("List:")
    for x in coll:
        print(f"* {x}")

def parse_first_request_line(s):
    (method, url, protocol) = s.split(" ")
    parsed_url = urlparse(url)
    return {"method": method,
            "url": url,
            "parsed_url": parsed_url,
            "protocol": protocol}

class ReqMeasurements:
    def __init__(self, req=None):
        if req is None:
            self.request_time = 0
            self.size = 0
            self.count = 0
        else:
            self.request_time = req["request-time"]
            self.size = req["size"]
            self.count = 1

    def __repr__(self):
        return f"ReqMeasurements(time={self.request_time}, size={self.size}, count={self.count})"

    def add(self, other):
        dst = ReqMeasurements()
        dst.request_time = self.request_time + other.request_time
        dst.size = self.size + other.size
        dst.count = self.count + other.count
        return dst

    def normalize(self):
        dst = ReqMeasurements()
        dst.request_time = self.request_time/self.count
        dst.size = self.size/self.count
        dst.count = 1
        return dst

    def avg_request_time(self):
        return self.request_time/self.count

    def avg_size(self):
        return self.size/self.count

def request_time(x):
    return x.request_time

def response_size(x):
    return x.size

class ReqAgg:
    def __init__(self, key, req):
        self.key = key
        self.req = req
        self.measurements = ReqMeasurements()

    def add(self, measurements):
        self.measurements = self.measurements.add(measurements)

    def __repr__(self):
        return f"ReqAgg({self.key}, {self.measurements})"

class AggregatedStats:
    def __init__(self, key_fn, aggregation_map):
        self._key_fn = key_fn
        self._aggregation_map = aggregation_map

    def sorted(self, sort_key):
        return list(sorted(self._aggregation_map.values(), key=lambda v: sort_key(v.measurements)))

    def filter_by_aggregation_key(self, k, log_stats):
        return [x for x in log_stats.vals() if self._key_fn(x) == k]

    def write_csv(self, dst_filename, reqaggs):
        p = Path(dst_filename)
        os.makedirs(p.parent, exist_ok=True)
        with open(p, "w") as f:
            cw = csv.writer(f)
            cw.writerow(["Path", "Query keys", "Example GraphQL", "Avg size", "Avg time", "Count", "Total size", "Total time", "Full example URL"])
            for x in reversed(reqaggs):
                (path, ks, _) = x.key
                graphql = req_stats_graphql(x.req) or ""
                m = x.measurements
                mhat = m.normalize()
                cw.writerow([path, ks, graphql, mhat.size, mhat.request_time, m.count, m.size, m.request_time, x.req.url])
    
class ReqStats:
    def __init__(self, req):
        frl = req["first-request-line"]
        parsed = parse_first_request_line(frl)
        self.example_request = req
        self.cache_key = req["cache-key"]
        self.first_request_line = frl
        self.method = parsed["method"]
        self.url = parsed["url"]
        self.parsed_url = parsed["parsed_url"]
        self.parsed_qs = parse_qs(self.parsed_url.query)
        self.protocol = parsed["protocol"]
        self.measurements = []
        self.total = ReqMeasurements()

    def __repr__(self):
        return f"ReqStats({self.cache_key} {self.total})"

    def add(self, req):
        m = ReqMeasurements(req)
        self.total = self.total.add(m)
        self.measurements.append(m)            
        
class LogStats:
    def __init__(self, key_req_map):
        self._key_req_map = key_req_map

    def vals(self):
        return list(sorted(self._key_req_map.values(),
                           key=lambda x: x.total.avg_request_time()))

    def all_request_times(self):
        return sorted([m.request_time
                       for x in self.vals()
                       for m in x.measurements])

    def display_time_distribution(self):
        times = self.all_request_times()
        n = len(times)
        samples = [0.0, 0.05, 0.25, 0.5, 0.75, 0.95, 1.0]
        print("Request times")
        for x in samples:
            print(f"At {int(100*x)}%: {times[round(x*(n-1))]}")

    def size_time_plot(self):
        vals = self.vals()
        X = [x.total.avg_size() for x in vals]
        Y = [x.total.avg_request_time() for x in vals]
        plt.scatter(X, Y)
        plt.show()

    def aggregate_stats(self, agg_key_fn):
        dst = {}
        for x in self.vals():
            k = agg_key_fn(x)
            if k not in dst:
                dst[k] = ReqAgg(k, x)
            dst[k].add(x.total.normalize())
        return AggregatedStats(agg_key_fn, dst)
    
def accumulate_stats(ds):
    dst = {}
    for i, x in enumerate(ds):
        if i % 50000 == 0:
            print(f"Accumulate at {i}...")
        if x["handling"] == "miss":
            k = x["cache-key"]
            if k not in dst:
                dst[k] = ReqStats(x)
            dst[k].add(x)
    return LogStats(dst)

def size_time_scatter_plot(results):
     X = [x.measurements.normalize().size for x in results]
     Y = [x.measurements.normalize().request_time for x in results]
     ax = plt.scatter(X, Y)
     plt.xscale("log")
     plt.yscale("log")
     plt.xlabel("Avg response size (bytes)")
     plt.ylabel("Avg time (seconds)")
     plt.show()

def minute_time_bucket_fn(bucket_size_minutes):
    def f(dt):
        dt = dt.replace(second=0, microsecond=0)
        bucket = dt.minute // bucket_size_minutes
        return dt.replace(minute=bucket*bucket_size_minutes)
    return f
     
def bucketed_request_times(bucket_fn, requests):
    dst = {}
    for x in requests:
        if x["handling"] == "miss":
            dt = x["timestamp"]
            k = bucket_fn(bucket_fn(dt))
            if k not in dst:
                dst[k] = []
            dst[k].append(x["request-time"])
    for k in dst:
        dst[k].sort()
    return dst

def frac_get(src, f):
    n = len(src)
    max_index = n-1
    return src[round(f*max_index)]

def plot_request_time_percentiles(rtime_map, fracs):
    X = sorted(rtime_map.keys())
    fig = plt.figure()
    for f in fracs:
        Y = [frac_get(rtime_map[k], f) for k in X]
        plt.plot(X, Y, label=f"{round(100*f)}%")
    plt.legend()
    plt.yscale("log")
    plt.xlabel("Request timestamp")
    plt.ylabel("Total request time (seconds)")
    plt.show()
        

if False: ##################################################################### Analysis around datahike deployment on 25
    bucket_fn = minute_time_bucket_fn(5)
    p = bucketed_request_times(bucket_fn, ds)
    plot_request_time_percentiles({dt:v for dt, v in p.items() if dt.hour in [8, 9, 10, 11]}, [0.1, 0.25, 0.5, 0.75, 0.9])

if False:
    agg_lower = analyze.parse_timestamp("2023-07-25T09:20:00.393Z")
    agg_upper = analyze.parse_timestamp("2023-07-25T09:50:00.393Z")
    subslice = [x
                for x in ds
                for dt in [x["timestamp"]]
                if agg_lower <= dt
                if dt <= agg_upper]
    acc = accumulate_stats(subslice)
    agg = acc.aggregate_stats(basic_aggregation_key)
    time_results = agg.sorted(request_time)
    size_results = agg.sorted(response_size)

if False:
    total_req_time = sum([x["request-time"] for x in subslice])
    
if False:
    dst_filename = rootdir.joinpath("size_time_analysis.csv")
    agg.write_csv(dst_filename, time_results)
    
if False:
    rootdir = analyze.location.overa
ll_root().joinpath("simulations", "sim3")
    ds = analyze.simulation_dataset(rootdir.joinpath("data"))
    x = next(iter(ds))
    print(ds)
















    
if False: ##################################################################### Analysis of six days
    agg = acc.aggregate_stats(basic_aggregation_key)
    time_results = agg.sorted(request_time)
    size_results = agg.sorted(response_size)

if False:
    time_results = agg.sorted(request_time)    
    size_time_scatter_plot(time_results)
    
if False:
    dst_filename = rootdir.joinpath("size_time_analysis.csv")
    agg.write_csv(dst_filename, time_results)
    
    #gql = agg.filter_by_aggregation_key(('/v1/taxonomy/graphql', 'query'), acc)
    #gql0 = gql[0]

    #graphql = gql0.parsed_qs["query"][0]
    #y = gparser.parse(graphql)

if False:
    
    n = 10000000
    acc = accumulate_stats(common.take_n(n, ds))
    
if False:
    fragment_queries = [x for x in acc.vals() if 0 < len(x.parsed_url.fragment)]
    param_queries = [x for x in acc.vals() if 0 < len(x.parsed_url.params)]

    print(f"Frag:  {len(fragment_queries)}")
    print(f"Param: {len(param_queries)}")

if False:
    rootdir = analyze.location.overall_root().joinpath("simulations", "sim1")
    ds = analyze.simulation_dataset(rootdir.joinpath("data"))
    x = next(iter(ds))
    print(ds)

    
