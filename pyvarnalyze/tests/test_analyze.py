import pyvarnalyze.analyze as a
import unittest
import datetime

def rec(k, size):
    return {"first-request-line": k, "size": size}

class TestPyvarnalyze(unittest.TestCase):
    def test_seconds_to_string(self):
        self.assertTrue(True)

    def test_time_sampler(self):
        offset = datetime.datetime.now()

        def t(x):
            return offset + datetime.timedelta(seconds=x)
        
        sampler = a.TimeSampler(2.0, 2.0)
        self.assertTrue(sampler.sample(t(0)))
        self.assertFalse(sampler.sample(t(0.1)))
        self.assertFalse(sampler.sample(t(0.9)))
        self.assertFalse(sampler.sample(t(1.1)))
        self.assertTrue(sampler.sample(t(2.1)))
        self.assertFalse(sampler.sample(t(2.1)))
        self.assertFalse(sampler.sample(t(5.1)))
        self.assertTrue(sampler.sample(t(6.1)))
        
