import pyvarnalyze.common as a
import unittest

def rec(k, size):
    return {"first-request-line": k, "size": size}

class TestCommon(unittest.TestCase):
    def test_seconds_to_string(self):
        self.assertEqual("2 hours, 2 seconds", a.seconds_to_string(7202))

    def test_format_size(self):
        self.assertEqual("3.16 KB", a.format_size(3234))
