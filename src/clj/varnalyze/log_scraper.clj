(ns varnalyze.log-scraper
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.data.json :as json]
            [clojure.pprint :as pp]
            [clojure.walk :refer [postwalk]]
            [clj-http.client :as client]))

(def url "https://opensearch.jobtechdev.se/internal/search/opensearch")

(defn get-http-method [h]
  (first (for [method ["GET" "POST" "UPDATE" "DELETE"]
               :when (str/starts-with? h (str method " "))]
           method)))

(defn parse-header [dst h]
  (if-let [method (get-http-method h)]
    (assoc dst :method method)
    (if-let [colon-pos (str/index-of h ": ")]
      (update dst :headers conj [(subs h 0 colon-pos) (subs h (+ 2 colon-pos))])
      (do (println "FAILED TO PARSE HEADER:" h)))))

(defn load-headers []
  (->> "query_headers.txt"
       io/resource
       slurp
       str/split-lines
       (reduce parse-header {:headers []})))

(defn load-query []
  (merge {:url url
          :body (-> "query_post_data.json"
                    io/resource
                    slurp
                    json/read-str)}
         (load-headers)))

(defn set-query-header [query k v]
  (update query
          :headers
          (fn [headers]
            (if (some #(= k (first %)) headers)
              (mapv (fn [[ok ov]]
                      (if (= ok k)
                        [ok v]
                        [ok ov]))
                    headers)
              (conj headers [k v])))))

(defn set-query-cookie [query cookie]
  (set-query-header query "Cookie" cookie))

(defn bound-params? [x]
  (and (map? x)
       (contains? x "gte")
       (contains? x "lte")))

(defn set-query-timebound [query gte lte]
  {:pre [(or (nil? gte) (string? gte))
         (or (nil? lte) (string? lte))]}
  (postwalk
   (fn [x]
     (if (bound-params? x)
       (into x (remove (comp nil? second)) [["gte" gte] ["lte" lte]])
       x))
   query))

(defn get-bound-params [query]
  (let [result (atom [])]
    (postwalk
     (fn [x]
       (when (bound-params? x)
         (swap! result conj x))
       x)
     query)
    (deref result)))


(def headers-to-ignore #{"Host" "Content-Length"})

(defn run-query [{:keys [method body headers url]}]
  (case method
    "POST" (client/post url
                        {:headers (into {} (remove (comp headers-to-ignore first)) headers)
                         :body (json/write-str body)})))

(defn parse-response [response]
  (update response :body json/read-str))

(defn response-hits [response]
  (get-in response [:body "rawResponse" "hits" "hits"]))

(defn hit-timestamp [hit]
  (first (get-in hit ["fields" "@timestamp"])))

(defn hit-id [hit]
  (get hit "_id"))

;; "2023-07-01T10:49:36.589Z"

(defn clean-timestamp-string [s]
  (-> s
      (str/replace #"-" "")
      (str/replace #":" "")
      (str/replace #"\." "_")))

(def sample-filename "data_n499_20230801T115059_911Z.json")

(def filename-pattern #"^data_n(\d+)_(\d+)T(\d+)_(\d+)Z\.json$")

(defn insert-seps [dst positions sep]
  (let [positions (set positions)]
    (apply str
           (into []
                 (map-indexed (fn [i c]
                                (if (contains? positions i)
                                  (str sep c)
                                  c)))
                 dst))))

(defn parse-data-filename [filename]
  (when-let [[_ n date time ms] (re-matches filename-pattern filename)]
    {:timestamp (str (insert-seps date [4 6] "-")
                     "T"
                     (insert-seps time [2 4] ":")
                     "."
                     ms
                     "Z")
     :count (Long/parseLong n)}))

(defn load-timestamp-bounds [output-directory]
  (let [output-directory (io/file output-directory)]
    (when (.exists output-directory)
      (let [timestamps (->> output-directory
                            .listFiles
                            (keep #(:timestamp (parse-data-filename (.getName %))))
                            sort)]
        (when (seq timestamps)
          [(first timestamps) (last timestamps)])))))

(defn essential-hit-data [hit]
  (let [source (get hit "_source")
        id (get hit "_id")
        timestamp (get source "@timestamp")
        message (get source "message")
        pod-name (get-in source ["kubernetes" "pod" "name"])]
    {:id id
     :timestamp timestamp
     :message message
     :pod pod-name}))

(defn download-data [query output-directory fill-gap]
  {:pre [(#{:newer :older} fill-gap)]}
  (println "Initial bounds:")
  (pp/pprint (get-bound-params query))
  (let [bounds (load-timestamp-bounds output-directory)
        query (if-let [[lower upper] bounds]
                (if (= fill-gap :older)
                  (do (println "Query up to" lower)
                      (set-query-timebound query nil lower))
                  (do (println "Query no older than " upper)
                      (set-query-timebound query upper nil)))
                (do
                  (println "No previous data found.")
                  query))]
    (loop [previous-hit-ids #{}
           counter 0
           query query
           saved-count 0]
      (if (and (< 0 counter)
               (empty? previous-hit-ids))
        (println "No more data")
        (let [response (-> query run-query parse-response)]
          (if (= 200 (:status response))
            (let [hits (remove (comp previous-hit-ids hit-id)
                                   (response-hits response))]
              (when (seq hits)
                (let [from-timestamp (hit-timestamp (last hits))
                      n (count hits)
                      filename (io/file output-directory
                                        (format "data_n%d_%s.json"
                                                n
                                                (clean-timestamp-string from-timestamp)))
                      new-query (set-query-timebound query nil from-timestamp)
                      saved-count (+ saved-count n)]
                  (println (format "Save %d hits from %s. Totally saved %d."
                                   (count hits) from-timestamp saved-count))
                  (io/make-parents filename)
                  (spit filename (json/write-str (map essential-hit-data hits)))
                  (recur (into #{} (map hit-id) hits)
                         (inc counter)
                         new-query
                         saved-count))))
            (throw (ex-info "Invalid response" {:response response
                                                :request query}))))))))


(defn scrape [output-directory fill-gap]
  (download-data (load-query) output-directory (keyword fill-gap)))

(comment

  (def base-query (load-query))
  (def query (-> base-query
                                        ;(set-query-timebound "2023-07-01T10:49:36.589Z" "2023-08-01T10:49:36.589Z")
                 ))

  (def response (parse-response (run-query query)))
  (def hits (response-hits response))
  (println "Number of hits: " (count hits))
  (println "Values from" (hit-timestamp (first hits)) "to" (hit-timestamp (last hits)))

  (download-data base-query "scraped_logs")
  )

