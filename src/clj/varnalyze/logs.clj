(ns varnalyze.logs
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.pprint :as pp]
            [clojure.set :as cljset]
            [clojure.data.json :as json]
            [varnalyze.log-scraper :as scraper])
  (:import [java.time LocalDateTime ZoneOffset Instant]
           [java.nio.file Path Paths Files]
           [java.io File]))

(defn instant? [x]
  (instance? Instant x))

(defmethod print-method Instant [v ^java.io.Writer w]
  (.write w (format "#Instant{\"%s\"}" (str v))))

(defn parse-timestamp [s]
  (Instant/parse s))

(def month-map (into {}
                     (comp (map-indexed (fn [index names] (for [name names] [name (inc index)])))
                           cat)
                     [["Jan"]
                      ["Feb"]
                      ["Mar"]
                      ["Apr"]
                      ["May" "Maj"]
                      ["Jun"]
                      ["Jul"]
                      ["Aug"]
                      ["Sep"]
                      ["Oct" "Okt"]
                      ["Nov"]
                      ["Dec"]]))

(def log-line-pattern #"^(\S*) .* \[(\p{Digit}+)/(.+)/(\p{Digit}+):(\p{Digit}+):(\p{Digit}+):(\p{Digit}+).*\] (pass|miss|hit) (\p{Digit}+) (\p{Digit}+) (\p{Digit}+) \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\".*$")

(def timestamp-ks [:day :month :year :hour :minute :second])

(def log-line-ks (into [] cat
                       [[:host]
                        timestamp-ks
                        [:handling :status :request-time :size :first-request-line :referer :user-agent :api-key]]))

(def log-line-ks-but-timestamp-ks (into [] (remove (set timestamp-ks)) log-line-ks))

(defn parse-int [x]
  (Integer/parseInt x))

(defn seconds-from-microseconds [x]
  (* 1.0e-6 x))

(defn parse-log-line [line]
  (when-let [[_ & parsed-line] (re-matches log-line-pattern line)]
    (assert (= (count parsed-line) (count log-line-ks)))
    (let [raw-map (zipmap log-line-ks parsed-line)
          {:keys [year month day hour minute second]} raw-map
          
          local-date-time (LocalDateTime/of (parse-int year)
                                            (month-map (:month raw-map))
                                            (parse-int day)
                                            (parse-int hour)
                                            (parse-int minute)
                                            (parse-int second))
          raw-map-without-time (select-keys raw-map log-line-ks-but-timestamp-ks)]
      (-> raw-map-without-time
          (assoc :epoch-seconds (.getEpochSecond (.toInstant local-date-time ZoneOffset/UTC)))
          (update :status parse-int)
          (update :size parse-int)
          (update :request-time (comp seconds-from-microseconds parse-int))))))

(defn path [first & more]
  (Paths/get first (into-array String more)))

(defn normalize-path [x]
  (cond
    (instance? Path x) x
    (instance? File x) (.toPath x)
    :default (path x)))

(defn parse-scraped-log-entry [entry]
  (let [{:keys [id message timestamp pod]} (into {} (for [[k v] entry] [(keyword k) v]))
        parsed (parse-log-line message)]
    (assoc parsed
           :has-data (boolean parsed)
           :message message
           :pod pod
           :id id
           :timestamp (parse-timestamp timestamp))))

(defn load-scraped-log-file
  ([filename] (load-scraped-log-file filename identity))
  ([filename xform]
   (->> filename
        normalize-path
        .toFile
        slurp
        json/read-str
        (into [] (comp (keep parse-scraped-log-entry) xform)))))

(defn update-if-exists [dst k f]
  (if (contains? dst k)
    (update dst k f)
    dst))

(defn parse-logs-to-json [source-dir target-file]
  (println "OBSOLETE!!!")
  #_(let [result (load-log-directory source-dir)]
      (with-open [w (io/writer target-file)]
        (json/write result w))))

(defn scraped-dataset [root-path0]
  (let [root-path (normalize-path root-path0)]
    {:log-files (sort-by :timestamp
                         (for [path (iterator-seq (.iterator (Files/list root-path)))
                               :let [parsed (-> path .getFileName str scraper/parse-data-filename)]
                               :when parsed]
                           (-> parsed
                               (assoc :path path)
                               (update :timestamp parse-timestamp))))
     :upper-bound nil
     :lower-bound nil}))

(defn dataset? [x]
  (and (map? x)
       (contains? x :log-files)))

(defn dataset-approx-count [ds]
  {:pre [(dataset? ds)]}
  (transduce (map :count)
             +
             0
             (:log-files ds)))



(defn instant< [^Instant a ^Instant b]
  (.isBefore a b))

(defn instant<= [a b]
  (not (instant< b a)))


(defn dataset-with-upper-bound [ds ^Instant upper]
  {:pre [(dataset? ds)
         (instant? upper)]}
  (-> ds
      (assoc :upper-bound upper)
      (update :log-files (fn [log-files] (filter #(instant<= (:timestamp %) upper) log-files)))))

(defn dataset-with-lower-bound [ds ^Instant lower]
  {:pre [(dataset? ds)
         (instant? lower)]}
  (-> ds
      (assoc :lower-bound lower)
      (update :log-files (fn [log-files]
                           (let [before (take-while #(instant< (:timestamp %) lower) log-files)]
                             (drop (max 0 (dec (count before))) log-files))))))

(defn dataset-summary [ds]
  (let [log-files (:log-files ds)
        rstr (fn [x] (str (:timestamp x)))]
    (format "Dataset of roughly %d records from %s to %s"
            (dataset-approx-count ds)
            (-> log-files first rstr)
            (-> log-files last rstr))))

(defn- dataset-step [dataset]
  (let [records (:records dataset)]
    (if (empty? records)
      (let [[log-file & log-files] (:log-files dataset)]
        (when log-file
          (let [ids (:ids dataset)
                records (load-scraped-log-file (:path log-file) (remove (comp ids :id)))]
            (assoc dataset
                   :current-log-file log-file
                   :log-files log-files
                   :records records
                   :ids (into #{} (map :id) records)))))
      (assoc dataset :records (rest records)))))

(defn first-record [state]
  (when-let [x (-> state :records first)]
    (assoc x :state state)))

(defn dataset-seq [ds]
  {:pre [(dataset? ds)]}
  (let [{:keys [upper-bound lower-bound]} ds]
    (->> (assoc ds :records [] :ids #{})
         (iterate dataset-step)
         (take-while some?)
         (keep first-record)
         (filter (fn [{:keys [^Instant timestamp]}]
                   (and (or (nil? upper-bound)
                            (instant<= timestamp upper-bound))
                        (or (nil? lower-bound)
                            (instant<= lower-bound timestamp)))))
         (map-indexed (fn [i x] (assoc x :counter i))))))

(defn log-record? [x]
  (and (map? x)
       (contains? x :id)))

(defn min-instant [a b]
  (if (instant< a b) a b))

(defn max-instant [a b]
  (if (instant< a b) b a))

(defn update-pod-data [pod-data x]
  (let [ts (:timestamp x)]
    (when (nil? ts)
      (println x))
    (if pod-data
      (-> pod-data
          (update :lower-timestamp min-instant ts)
          (update :upper-timestamp max-instant ts)
          (update :counter inc))
      {:counter 1
       :lower-timestamp ts
       :upper-timestamp ts})))

(defn- analyze-record-acc [{:keys [period]} analysis x]
  {:pre [(log-record? x)]}
  (let [counter (:counter x)]
    (when (and (not (zero? period))
               (zero? (mod counter period)))
      (println (format "Loaded record %d" counter))))
  
  (-> analysis
      (update-in [:pods (:pod x)] update-pod-data x)
      (update :count inc)))

(defn analyze-records
  ([records] (analyze-records records {}))
  ([records settings]
   (let [settings (merge {:period 10000} settings)]
     (reduce (partial analyze-record-acc settings)
             {:count 0
              :pods {}}
             records))))

(defn analysis-list-pods [analysis]
  (vec (sort-by (comp :lower-timestamp second) (:pods analysis))))

(defn filter-by-pod [pod-pred & args]
  (apply filter (fn [record] (pod-pred (:pod record))) args))

(defn testf0 [x]
  (<= x 11))

(defn- init-range [f]
  (loop [l 0
         u 1]
    (if (f u)
      (recur u (* 2 u))
      [l u])))

(defn- narrow-range [[l u] f]
  (loop [l l
         u u]
    (if (= 1 (- u l))
      l
      (let [m (quot (+ l u) 2)]
        (if (f m)
          (recur m u)
          (recur l m))))))

(defn find-max-int [f]
  (when (f 0)
    (narrow-range
     (init-range f) f)))

#_(defn drop-old-pods [ds]
  (let [log-files (vec (:log-files ds))
        n (count log-files)
        normalize-index (fn [i] (- n 1 i))
        load-pod-set (fn [index]
                       (let [index (normalize-index index)]
                         (if (<= 0 index)
                           (into #{}
                                 (comp (map :pod))
                                 (load-scraped-log-file (:path (nth log-files index))))
                           #{})))
        last-pods (load-pod-set 0)
        f (fn [i]
            (cljset/subset?
             last-pods
             (load-pod-set i)))
        lower (find-max-int f)]
    (println "\n\n\n\n\n")
    (println "Lower pods" (load-pod-set lower))
    (println "Upper pods" last-pods)
    (assoc ds :log-files (subvec log-files lower))))

(comment

  (def ds (scraped-dataset "scraped_logs"))
  (def ds0 (drop-old-pods ds))
  (println "Count:" (dataset-approx-count ds))
  (println (analyze-records (take 10000 (dataset-seq ds))))

  (def full-analysis (analyze-records (dataset-seq ds)))


  )
