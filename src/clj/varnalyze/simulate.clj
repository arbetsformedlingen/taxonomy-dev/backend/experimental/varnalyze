(ns varnalyze.simulate
  (:require [clojure.string :as str]
            [clojure.data.json :as json]
            [clojure.java.io :as io]
            [varnalyze.logs :as logs]
            [clojure.pprint :as pp])
  (:import [java.nio.file Path Files]
           [varnalyze
            IBucket
            BasicBucket
            BranchBucket
            Request
            BucketData
            Handling$Type]))

(def size-overhead 600)

(defn bucket? [x]
  (instance? IBucket x))

(defn basic-bucket
  ([size size-overhead]
   {:pre [(number? size)
          (number? size-overhead)]}
   (BasicBucket. (long size) (long size-overhead)))
  ([size]
   (basic-bucket size 0)))

(defn branch-bucket [key-fn sub-buckets]
  {:pre [(instance? clojure.lang.IFn key-fn)
         (map? sub-buckets)]}
  (BranchBucket. key-fn sub-buckets))



(defn request [{:keys [first-request-line
                       size
                       timestamp
                       api-key] :as reqdata}]
  {:pre [(string? first-request-line)
         (number? size)
         (logs/instant? timestamp)]}
  (Request. (str first-request-line "---" size "---" api-key)
            (long size)
            timestamp
            reqdata
            Request/YEAR_LIFETIME))

(defn classify-by-size [bucket-cfg size]
  {:pre [(number? size)]}
  (first (for [{:keys [lower upper name]} bucket-cfg
               :when (and (<= lower size)
                          (< size upper))]
           name)))

(defn size-partitioning [cfg]
  {:pre [(sequential? cfg)]}
  (branch-bucket
   #(classify-by-size cfg (.size ^Request %))
   (into {}
         (for [{:keys [name capacity]} cfg]
           [name (basic-bucket capacity size-overhead)]))))

(def batch-writer 
  {:current-path nil
   :buffer []})

(defn batch-writer-finalize [{:keys [current-path buffer]}]
  (when current-path
    (spit (.toFile (logs/normalize-path current-path))
          (json/write-str buffer))))

(defn batch-writer-add [{:keys [current-path buffer] :as writer} path x]
  (if (or (nil? current-path) (= path current-path))
    {:current-path path
     :buffer (conj buffer x)}
    (do (batch-writer-finalize writer)
        (let [ret {:current-path path
                   :buffer [x]}]
          ret))))

(defn simulation [output-path bucket records]
  {:pre [(bucket? bucket)
         (sequential? records)]}
  {:output-path (logs/normalize-path output-path)
   :bucket bucket
   :records records})

(defn simulation-step
  ([{:keys [writer]}] (batch-writer-finalize writer))
  ([{:keys [out-path writer bucket period counter] :as sim-state} record]
   (when (zero? (mod counter period))
     (println "Simulation step" counter))
   (let [req (request record)
         handling (.addRequest bucket req [])
         current-log-file (-> record :state :current-log-file :path .getFileName str)
         out-file (io/file out-path current-log-file)
         exported-record (-> record
                             (assoc :cache-key (.key req))
                             (dissoc :state :has-data :message)
                             (update :timestamp str)
                             (assoc :simulation-handling (.makeData handling))
                             (assoc :simulation-bucket-data (.makeData (.getData bucket))))]
     (-> sim-state
         (assoc :writer (batch-writer-add writer out-file exported-record))
         (update :counter inc)))))

(def valid-handlings #{"hit" "miss" "pass"})
(def sim-handlings #{"hit" "miss"})

(defn simulate-record? [{:keys [has-data handling]}]
  {:pre [(boolean? has-data)
         (or (not has-data) (valid-handlings handling))]}
  (and has-data (sim-handlings handling)))

(defn run-simulation [{:keys [^Path output-path bucket records]}]
  {:pre [output-path
         (bucket? bucket)
         (sequential? records)]}
  (let [output-path (.toFile output-path)
        cfgfile (io/file output-path "bucket_config.json")]
    (println "Make the path" output-path)
    ;(.mkdir output-path)
    (io/make-parents cfgfile)
    (assert (.exists output-path))
    (spit cfgfile (json/write-str (.configData bucket)))
    (transduce (filter simulate-record?)
               simulation-step
               {:writer batch-writer
                :bucket (.replicate bucket)
                :out-path output-path
                :counter 0
                :period 10000}
               records)))

(defn prepare-dataset-for-simulation
  ([dataset] (prepare-dataset-for-simulation dataset nil))
  ([dataset pod-keys]
   {:pre [(logs/dataset? dataset)]}
   (println "Prepare dataset:\n" (logs/dataset-summary dataset))
   (let [min-count-per-pod 100
         analysis (-> dataset logs/dataset-seq logs/analyze-records)
         pod-keys (or pod-keys
                      (into #{}
                            (comp (filter (fn [[_k pod-data]] (< min-count-per-pod (:counter pod-data))))
                                  (map first))
                            (:pods analysis)))]
     {:dataset dataset
      :pod-keys pod-keys
      :analysis analysis
      :records (->> dataset
                    logs/dataset-seq
                    (logs/filter-by-pod pod-keys)
                    )})))

(defn pod-bucket [pod-keys prototype]
  {:pre [(bucket? prototype)]}
  (branch-bucket
   #(:pod (.data %))
   (into {}
         (for [podkey pod-keys]
           [podkey (.replicate prototype)]))))

(defn run-simulation0 [prep bucket-cfg]
  (run-simulation (simulation "simulations/testsim0"
                              (pod-bucket
                               (:pod-keys prep)
                               (size-partitioning bucket-cfg))
                              (:records prep))))

(def megabytes (* 1024 1024))

(defn run-simulation1 [prep bucket-cfg]
  (run-simulation
   (simulation "simulations/sim1/data"
               (pod-bucket
                (:pod-keys prep)
                (size-partitioning
                 (for [c bucket-cfg]
                     (assoc c :capacity (* 1000 megabytes)))))
               (:records prep))))

(defn run-basic-simulation [outdir prep bucket-cfg]
  (run-simulation
   (simulation outdir
               (pod-bucket
                (:pod-keys prep)
                (size-partitioning bucket-cfg))
               (:records prep))))



(comment

  ;; Export for the six days after we went back to Datomic
  (def prep (-> "scraped_logs"
                logs/scraped-dataset
                (logs/dataset-with-lower-bound
                 (logs/parse-timestamp "2023-07-27T10:00:00.939Z"))
                #_(logs/dataset-with-upper-bound
                   (logs/parse-timestamp "2023-07-27T12:00:23.939Z"))
                prepare-dataset-for-simulation))

  (def cfg (-> "../openshift-varnish/buckets/bucket_data.json"
               slurp
               (json/read-str {:key-fn keyword})))

  (run-simulation1 prep cfg)
  )

(comment
  ;; Export simulation around the Datahike experiment.
  (def prep (-> "scraped_logs"
                logs/scraped-dataset
                (logs/dataset-with-lower-bound
                 (logs/parse-timestamp "2023-07-25T06:00:00.393Z"))
                (logs/dataset-with-upper-bound
                 (logs/parse-timestamp "2023-07-25T18:00:00.683Z"))
                (prepare-dataset-for-simulation)))

  (def cfg (-> "../openshift-varnish/buckets/bucket_data.json"
               slurp
               (json/read-str {:key-fn keyword})))

  (run-basic-simulation "simulations/sim3/data" prep cfg)


  )




(defn simulate [^IBucket bucket requests]
  (into []
        (map (fn [^Request request]
               (let [h (.addRequest bucket request [])]
                 {:request request
                  :handling h
                  :bucket-data (.getData bucket)})))
        requests))

(defn handling-type-str [^Handling$Type x]
  (-> x str str/lower-case))
