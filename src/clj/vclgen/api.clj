(ns vclgen.api
  (:require [varnalyze.logs :refer [parse-logs-to-json]]
            [varnalyze.log-scraper :refer [scrape]]))

(defn -main [op & args]
  (case op
    "parse-logs" (apply parse-logs-to-json args)
    "scrape" (apply scrape args)))
