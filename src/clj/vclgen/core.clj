(ns vclgen.core
  (:require [clojure.spec.alpha :as spec]
            [clojure.string :as cljstr]
            [clojure.edn :as edn]
            [vclgen.test :as test]
            [clojure.java.io :as io]
            [clojure.data.json :as json]))

(defn megabytes [i]
  (* i 1024 1024))

(defn parse-size [[value size-unit]]
  (* value (case size-unit
             :mb (* 1024 1024)
             :kb 1024
             :b 1)))

(defn load-config [filename]
  (-> filename
      slurp
      edn/read-string
      (update :first-threshold parse-size)
      (update :max-response parse-size)
      (update :min-storage parse-size)
      (update :max-storage parse-size)))


(defn clamp [x lower upper]
  (cond
    (< x lower) lower
    (< upper x) upper
    :default x))

(defn generate-buckets [{:keys [first-threshold span-factor max-response max-storage capacity-factor min-storage]}]
  (->> {:lower 0
        :upper first-threshold}
       (iterate (fn [{:keys [upper]}]
                  {:lower upper
                   :upper (min max-storage (* span-factor upper))}))
       (take-while (fn [{:keys [lower upper]}]
                     (and (<= lower max-response)
                          (< lower max-storage))))
       (map-indexed (fn [i bucket]
                      (assoc bucket
                             :capacity (clamp (* (:upper bucket) capacity-factor)
                                              min-storage
                                              max-storage)
                             :name (format "bucket%d" i))))))

(defn round-int [x]
  (-> x double Math/round int))

(defn bucket-str [{:keys [name lower upper capacity]}]
  (format " * %s: [%d, %d[ , capacity=%d MB" name lower upper (round-int (/ capacity (megabytes 1)))))

(defn strategies-str [buckets]
  (let [total-storage (apply + (map :capacity buckets))]
    (cljstr/join
     "\n"
     (into [(format "%d buckets with a total storage of %d MB:" (count buckets)
                    (round-int (/ total-storage (megabytes 1))))]
           (map bucket-str)
           buckets))))



;; How to use multiple storages
;; https://info.varnish-software.com/blog/partitioning-your-varnish-cache
;;
;; Details about the storage parameters
;; https://varnish-cache.org/docs/trunk/users-guide/storage-backends.html
(defn render-buckets-cli [buckets]
  (cljstr/join " "
               (for [bucket buckets]
                 (format "-s %s=default,%s"
                         (:name bucket)
                         (:capacity bucket)))))

(defn indent-lines [indentation s]
  (->> s
       cljstr/split-lines
       (map (partial str indentation))
       (cljstr/join "\n")))

;; `storage_hint` was deprecated, use `storage` instead:
;; https://varnish-cache.org/docs/6.0/whats-new/upgrading-6.0.html#variable-changes-in-vcl-4-0-and-4-1
;;
;; Deprecated syntax to choose storage:
;; set beresp.storage_hint = \"%s\";
;;
;; Some notes on the beresp.storage vs beresp.storage_hint
;; https://silvermou.se/varnish-expression-has-type-string-expected-stevedore/
;;
;; Example of using a storage backend and addressing it by name.
;; https://varnish-cache.org/docs/6.0/reference/varnishd.html#storage-backend


(defn render-buckets-vcl [buckets]
  (str "# This code is generated.
# The code classifies requests into buckets of requests with about the same size.\n\n"
       (indent-lines "# " (strategies-str buckets))
       "\n\n"
       (cljstr/join
        " "
        (map-indexed
         (fn [i bucket]
           (format "%s (std.integer(beresp.http.Content-Length, 0) < %d) {
  set beresp.storage = storage.%s;
  set beresp.http.x-storage = \"%s\";
}"
                   (if (zero? i) "if" "elsif")
                   (:upper bucket)
                   (:name bucket)
                   (:name bucket)))
         buckets))
       " else {
  set beresp.ttl = 0s;
  set beresp.grace = 0s;
  set beresp.uncacheable = true;
  return (deliver);
}"))



(defn produce [{:keys [buckets-config-file output-root]}]
  (let [config (load-config buckets-config-file)
        buckets (generate-buckets config)

        vcl-file (io/file output-root "buckets.vcl")
        storage-file (io/file output-root "storages.txt")
        bucket-data-file (io/file output-root "bucket_data.json")]
    (println (strategies-str buckets))
    (io/make-parents vcl-file)
    (spit vcl-file (indent-lines "  " (render-buckets-vcl buckets)))
    (spit storage-file (render-buckets-cli buckets))
    (with-open [w (io/writer bucket-data-file)]
      (json/write buckets w))))

(def test-api test/test-api)

(comment
  (do

    (produce {:buckets-config-file "buckets_config.edn"
              :output-root "mjaotmp"})

    ))
