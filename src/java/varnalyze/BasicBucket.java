package varnalyze;

import java.util.HashMap;
import java.util.TreeMap;
import clojure.lang.RT;
import clojure.lang.IPersistentCollection;
import java.lang.Comparable;
import clojure.lang.Keyword;
import clojure.lang.PersistentHashMap;

public class BasicBucket implements IBucket {
    private long _size = 0;
    private long _used = 0;

    private long _insertedCount = 0;
    private long _nukedCount = 0;
    private long _insertedBytes = 0;
    private long _nukedBytes = 0;
    private long _sizeOverhead = 0;
    
    private long _counter = 0;
    private TreeMap<CounterKey, Long> _itemSet;
    private HashMap<String, CounterKey> _itemMap;

    private class CounterKey implements Comparable<CounterKey> {
        public long counter;
        public String key;
        
        public CounterKey(long c, String k) {
            counter = c;
            key = k;
        }

        public int compareTo(CounterKey other) {
            if (counter < other.counter) {
                return -1;
            } else if (counter > other.counter) {
                return 1;
            }
            return key.compareTo(other.key);
        }
    }
    
    public BasicBucket(long size, long sizeOverhead) {
        _size = size;
        _sizeOverhead = sizeOverhead;
        _used = 0;
        _counter = 0;
        _itemSet = new TreeMap<CounterKey, Long>();
        _itemMap = new HashMap<String, CounterKey>();
    }

    public Object configData() {
        return PersistentHashMap.create(
            Keyword.intern("size"), _size,
            Keyword.intern("size-overhead"), _sizeOverhead);
    }

    public IBucket replicate() {
        return new BasicBucket(_size, _sizeOverhead);
    }

    private long incCounter() {
        long i = _counter;
        _counter++;
        return i;
    }

    private void nuke() {
        CounterKey ck = _itemSet.firstKey();
        _nukedCount++;
        _nukedBytes += remove(ck);
    }

    private long remove(CounterKey ck) {
        long size = _itemSet.get(ck).longValue();
        _itemSet.remove(ck);
        _itemMap.remove(ck.key);
        _used -= size;
        return size;
    }
    
    private long remove(String key) {
        return remove(_itemMap.get(key));
    }
    
    private void insert(Request r) {
        CounterKey ck = new CounterKey(incCounter(), r.key());
        _itemSet.put(ck, Long.valueOf(r.size()));
        _itemMap.put(r.key(), ck);
        _used += r.size();
    }

    public Handling addRequest(Request r0, IPersistentCollection path) {
        Request r = r0.addToSize(_sizeOverhead);
        CounterKey ck = _itemMap.get(r.key());
        BucketData data = getData();
        if (ck == null) {
            while (_size < _used + r.size()) {
                nuke();
            }
            _insertedCount += 1;
            _insertedBytes += r.size();
            insert(r);
            return Handling.miss(path, data);
        } else {
            remove(ck);
            insert(r);
            return Handling.hit(path, data);
        }
    }

    public BucketData getData() {
        BucketData dst = new BucketData();
        dst.bucketCount = 1;
        dst.size = _size;
        dst.used = _used;
        dst.insertedCount = _insertedCount;
        dst.insertedBytes = _insertedBytes;
        dst.nukedCount = _nukedCount;
        dst.nukedBytes = _nukedBytes;
        return dst;
    }
}
