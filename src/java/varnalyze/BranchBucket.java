package varnalyze;

import clojure.lang.IFn;
import clojure.lang.RT;
import clojure.lang.Keyword;
import clojure.lang.IPersistentCollection;
import clojure.lang.PersistentHashMap;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;

public class BranchBucket implements IBucket {
    private IFn _keyFn;
    private Map<Object, IBucket> _subBuckets;

    public BranchBucket(IFn keyFn, Map<Object, IBucket> subBuckets) {
        _keyFn = keyFn;
        _subBuckets = subBuckets;
    }
    
    public Handling addRequest(Request r, IPersistentCollection path) {
        Object key = _keyFn.invoke(r);
        IBucket bucket = _subBuckets.get(key);
        if (bucket == null) {
            throw new RuntimeException("No bucket at key " + key.toString());
        }
        return bucket.addRequest(r, RT.conj(path, key));
    }

    public BucketData getData() {
        BucketData dst = new BucketData();
        for (IBucket b: _subBuckets.values()) {
            dst = dst.add(b.getData());
        }
        return dst;
    }

    public IBucket replicate() {
        HashMap<Object, IBucket> dst = new HashMap();
        for (var kv: _subBuckets.entrySet()) {
            dst.put(kv.getKey(), kv.getValue().replicate());
        }
        return new BranchBucket(_keyFn, dst);
    }

    public Object configData() {
        Object dst = PersistentHashMap.EMPTY;
        for (var kv: _subBuckets.entrySet()) {
            dst = RT.assoc(dst, kv.getKey(), kv.getValue().configData());
        }
        return PersistentHashMap.create(Keyword.intern("sub-buckets"), dst);
    }
}
