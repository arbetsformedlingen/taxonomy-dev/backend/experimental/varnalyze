package varnalyze;

import clojure.lang.Keyword;
import clojure.lang.PersistentHashMap;

public class BucketData {
    public long bucketCount = 0;
    public long size = 0;
    public long used = 0;
    public long insertedCount = 0;
    public long insertedBytes = 0;
    public long nukedCount = 0;
    public long nukedBytes = 0;
        

    public static Keyword SIZE = Keyword.intern("size");
    public static Keyword USED = Keyword.intern("used");
    public static Keyword BUCKET_COUNT = Keyword.intern("bucket-count");
    public static Keyword INSERTED_COUNT = Keyword.intern("inserted-count");
    public static Keyword INSERTED_BYTES = Keyword.intern("inserted-bytes");
    public static Keyword NUKED_COUNT = Keyword.intern("nuked-count");
    public static Keyword NUKED_BYTES = Keyword.intern("nuked-bytes");
    
    public PersistentHashMap makeData() {
        return PersistentHashMap.create(
            USED, used,
            SIZE, size,
            BUCKET_COUNT, bucketCount,
            INSERTED_COUNT, insertedCount,
            INSERTED_BYTES, insertedBytes,
            NUKED_COUNT, nukedCount,
            NUKED_BYTES, nukedBytes);
    }

    public BucketData add(BucketData other) {
        BucketData dst = new BucketData();
        dst.size = size + other.size;
        dst.used = used + other.used;
        dst.bucketCount = bucketCount + other.bucketCount;
        dst.insertedCount = insertedCount + other.insertedCount;
        dst.insertedBytes = insertedBytes + other.insertedBytes;
        dst.nukedCount = nukedCount + other.nukedCount;
        dst.nukedBytes = nukedBytes + other.nukedBytes;
        return dst;
    }
}
