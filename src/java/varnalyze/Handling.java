package varnalyze;

import clojure.lang.IPersistentCollection;
import clojure.lang.PersistentHashMap;
import clojure.lang.Keyword;

public class Handling {
    public enum Type {
        Hit,
        Miss
    }

    public static Keyword TYPE = Keyword.intern("type");
    public static Keyword PATH = Keyword.intern("path");
    public static Keyword DATA = Keyword.intern("data");

    private Type _type;
    private IPersistentCollection _path;
    private BucketData _data;

    public Handling(Type type, IPersistentCollection path, BucketData data) {
        _type = type;
        _path = path;
        _data = data;
    }

    public static Handling hit(IPersistentCollection path, BucketData data) {
        return new Handling(Type.Hit, path, data);
    }
    
    public static Handling miss(IPersistentCollection path, BucketData data) {
        return new Handling(Type.Miss, path, data);
    }

    public Type getType() {
        return _type;
    }

    public IPersistentCollection getPath() {
        return _path;
    }

    public BucketData getData() {
        return _data;
    }

    public Object makeData() {
        return PersistentHashMap.create(
            TYPE, _type.toString().toLowerCase(),
            PATH, _path,
            DATA, _data.makeData());
    }
}
