package varnalyze;

import java.util.HashMap;
import java.util.SortedMap;
import clojure.lang.IPersistentCollection;

public interface IBucket {
    public Handling addRequest(Request r, IPersistentCollection path);
    public BucketData getData();
    public IBucket replicate();
    public Object configData();
}
