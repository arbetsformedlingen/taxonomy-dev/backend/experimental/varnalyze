package varnalyze;

import java.time.Instant;
import java.util.Map;

public class Request {
    private String _key;
    private long _size;
    private Instant _timestamp;
    private Object _data;
    private double _timeToLiveSeconds;

    public static double YEAR_LIFETIME = 365.0*24.0*60.0*60.0;
    
    public Request(String key, long size, Instant timestamp, Object data, double timeToLiveSeconds) {
        _key = key;
        _size = size;
        _timestamp = timestamp;
        _data = data;
        _timeToLiveSeconds = timeToLiveSeconds;
    }

    public Request addToSize(long extra) {
        return new Request(_key, _size + extra, _timestamp, _data, _timeToLiveSeconds);
    }
    
    public String key() {return _key;}
    public long size() {return _size;}
    public Instant timestamp() {return _timestamp;}
    public Object data() {return _data;}
    public Instant deathTimestamp() {return _timestamp.plusMillis((long)(1000*_timeToLiveSeconds));}
    
}
