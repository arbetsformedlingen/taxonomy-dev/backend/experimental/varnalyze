(ns varnalyze.logs-test
  (:require [clojure.test :refer :all]
            [varnalyze.logs :refer :all])
  (:import [java.time Instant]))

(def example0 ["api.jobtech-taxonomy-api-prod-read.svc.cluster.local 10.131.0.111 - - [27/Jul/2023:10:00:42 +0000] miss 200 206470 217167 \"GET http://taxonomy.api.jobtechdev.se:443/v1/taxonomy/specific/concepts/ssyk?relation=broader&version=1 HTTP/1.1\" \"-\" \"Apache-HttpClient/4.5.13 (Java/1.8.0_372)\" \"-\""
               {:request-time 0.206470,
                :referer "-",
                :user-agent "Apache-HttpClient/4.5.13 (Java/1.8.0_372)",
                :size 217167,
                :host "api.jobtech-taxonomy-api-prod-read.svc.cluster.local",
                :handling "miss",
                :status 200,
                :first-request-line
                "GET http://taxonomy.api.jobtechdev.se:443/v1/taxonomy/specific/concepts/ssyk?relation=broader&version=1 HTTP/1.1",
                :epoch-seconds 1690452042,
                :api-key "-"}])

(deftest test-parse-log-line
  (let [[input output] example0]
    (is (= (parse-log-line input) output))))

(deftest scraped-dataset-test
  (is (instance? Instant (parse-timestamp "2023-08-01T13:04:04.965Z"))))

;; PASS happens when the request is not meant to be cached, e.g. due to `autocomplete`.
(def example1 "api.jobtech-taxonomy-api-prod-read.svc.cluster.local 10.131.0.111 - - [28/Jul/2023:09:42:40 +0000] pass 200 24472 2 \"GET http://taxonomy.api.jobtechdev.se:443/v1/taxonomy/suggesters/autocomplete?type=keyword&query-string=strip&version=1 HTTP/1.1\" \"-\" \"AHC/1.0\" \"-\"")

(deftest test-parse-pass
  (is (map? (parse-log-line example1))))

(deftest test-find-max-int
  (dotimes [i 100]
    (is (= i (find-max-int #(<= % i))))))
