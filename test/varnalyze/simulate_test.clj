(ns varnalyze.simulate-test
  (:require [clojure.test :refer :all]
            [varnalyze.simulate :refer :all]
            [varnalyze.logs :as logs]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.data.json :as json])
  (:import [varnalyze
            BasicBucket
            BucketData
            Request
            BranchBucket
            Handling$Type]
           [java.time Instant]
           [java.nio.file Paths Files LinkOption Path]
           [java.nio.file.attribute FileAttribute]
           [java.io File]))

(defn test-timestamp [seconds]
  (.plusMillis (logs/parse-timestamp "2023-08-01T13:04:04.965Z")
               (long (* 1000.0 seconds))))

(deftest test-bucket-from-spec
  (let [bucket (basic-bucket 100)]
    (is (instance? BasicBucket bucket))
    (is (= {:size 100 :used 0
            :bucket-count 1

            :inserted-count 0
            :inserted-bytes 0

            :nuked-count 0
            :nuked-bytes 0}
           (.makeData (.getData bucket)))))
  (let [bucket (branch-bucket :pod {"pod-a" (basic-bucket 100)
                                    "pod-b" (basic-bucket 100)})]
    (is (instance? BranchBucket bucket))
    (is (= {:size 200
            :used 0
            :bucket-count 2

            :inserted-count 0
            :inserted-bytes 0

            :nuked-count 0
            :nuked-bytes 0}
           (.makeData (.getData bucket))))))

(def test-data [[["a" 4] 4 "miss"]
                [["a" 4] 4 "hit"]
                [["a" 4] 4 "hit"]
                [["b" 4] 8 "miss"]
                [["b" 4] 8 "hit"]
                [["a" 4] 8 "hit"]
                [["c" 4] 8 "miss"]
                [["c" 4] 8 "hit"]
                [["a" 4] 8 "hit"]
                [["b" 4] 8 "miss"]
                [["c" 4] 8 "miss"]])

(deftest test-simulate-it
  (let [input (for [[[k size]] test-data]
                (request {:first-request-line k
                          :size size
                          :pod ""
                          :timestamp (test-timestamp 0)}))
        results (simulate (basic-bucket 10) input)]
    (doseq [[[_ used h] {:keys [handling request bucket-data]}] (map vector test-data results)]
      (let [bucket-data (.makeData bucket-data)]
        (is (= used (:used bucket-data)))
        (is (= h (-> handling .getType handling-type-str)))))))

(defn create-temp-dir []
  (Files/createTempDirectory "varnalyze" (into-array FileAttribute [])))

(deftest test-batch-writer
  (let [d (.toFile (create-temp-dir))
        load-file (fn [f] (-> d
                              (io/file f)
                              slurp
                              (json/read-str {:key-fn keyword})))]
    (-> batch-writer
        (batch-writer-add (io/file d "a.json") {:data 0})
        (batch-writer-add (io/file d "a.json") {:data 1})
        (batch-writer-add (io/file d "b.json") {:data 2})
        (batch-writer-add (io/file d "c.json") {:data 3})
        batch-writer-finalize
        )
    (is (= [{:data 0}
            {:data 1}]
           (load-file "a.json")))
    (is (= [{:data 2}] (load-file "b.json")))
    (is (= [{:data 3}] (load-file "c.json")))))
